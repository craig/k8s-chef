FROM ubuntu:18.04
MAINTAINER craig@gitlab.com

ENV DEBIAN_FRONTEND noninteractive
ENV VERSION="12.19.31"

RUN apt-get update && \
    apt-get install -yq --no-install-recommends cron curl iproute2 rsync tzdata vim wget && \
    wget --no-check-certificate "https://packages.chef.io/files/stable/chef-server/${VERSION}/ubuntu/18.04/chef-server-core_${VERSION}-1_amd64.deb" && \
    dpkg -i chef-server*.deb && \
    rm chef-server*.deb && \
    apt-get remove -y wget && \
    rm -rf /var/lib/apt/lists/*
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

COPY main.sh chef.sh /usr/local/bin/

VOLUME /var/log

EXPOSE 80
EXPOSE 443
EXPOSE 9683

CMD ["main.sh"]
