#!/bin/bash

# Create chef-server.rb with variables
cat <<-EOF > /etc/opscode/chef-server.rb
nginx['enable_non_ssl']              = false
nginx['ssl_port']                    = ${SSL_PORT:-443}
nginx['server_name']                 = "${CONTAINER_NAME:-chef-server}"
postgresql['db_superuser']           = "${POSTGRES_SUPERUSER:-superuser_userid}"
postgresql['db_superuser_password']  = "${POSTGRES_SUPERUSER:-ch@ng3me!}"
postgresql['external']               = "${POSTGRES_EXTERNAL:-false}"
postgresql['port']                   = "${POSTGRES_PORT:-5432}"
postgresql['vip']                    = "${POSTGRES_HOST:-postgres}"
EOF

if [[ ! -d /etc/init ]]; then
    mkdir /etc/init
fi

echo -e "\nRunning: 'chef-server-ctl reconfigure'. This step will take a few minutes..."
export PATH=/opt/opscode/bin:/opt/opscode/embedded/bin:$PATH
/opt/opscode/embedded/bin/runsvdir-start &
chef-server-ctl reconfigure

URL="http://127.0.0.1:8000/_status"
CODE=1
SECONDS=0
TIMEOUT=60

return=$(curl -sf ${URL})

if [[ -z "$return" ]]; then
  echo -e "\nINFO: Chef-Server isn't ready yet!"
  echo -e "Blocking until <${URL}> responds...\n"

  while [ $CODE -ne 0 ]; do

    curl -sf \
         --connect-timeout 3 \
         --max-time 5 \
         --fail \
         --silent \
         ${URL}

    CODE=$?

    sleep 2
    echo -n "."

    if [ $SECONDS -ge $TIMEOUT ]; then
      echo "$URL is not available after $SECONDS seconds...stopping the script!"
      exit 1
    fi
  done;
fi

echo -e "\n\n$URL is available!\n"
echo -e "\nSetting up admin user and default organization"
# chef-server-ctl user-create USER_NAME FIRST_NAME [MIDDLE_NAME] LAST_NAME EMAIL 'PASSWORD' (options)
chef-server-ctl user-create gstg-admin 'GitLab Staging' 'Admin Account' ops-contact+chef@gitlab.com 'placeholder@123' --filename /etc/chef/gstg-admin.pem
chef-server-ctl org-create gstg 'GitLab Staging' --association_user gstg-admin --filename /etc/chef/gstg-validator.pem

echo "{ \"error\": \"Please use https:// instead of http:// !\" }" > /var/opt/opscode/nginx/html/500.json
sed -i "s,/503.json;,/503.json;\n    error_page 497 =503 /500.json;,g" /var/opt/opscode/nginx/etc/chef_https_lb.conf
sed -i '$i\    location /knife_admin_key.tar.gz {\n      default_type application/zip;\n      alias /etc/chef/knife_admin_key.tar.gz;\n    }' /var/opt/opscode/nginx/etc/chef_https_lb.conf

echo -e "\nCreating tar file with the Knife keys"
cd /etc/chef/ && tar -cvzf knife_admin_key.tar.gz gstg-admin.pem gstg-validator.pem

echo -e "\nRestart Nginx..."
chef-server-ctl restart nginx
chef-server-ctl status

touch /root/chef_configured
echo -e "\n\nDone!\n"
