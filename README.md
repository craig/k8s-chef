# docker-chef-server

This project will build a docker container with Chef Infra Server.

## Usage

The container must be executed in privileged mode (for sysctl settings), and requires a valid FQDN
be resolvable locally within the container.

```
docker run -d --privileged -h chef.fq.dn gitlab/chef-server
```
